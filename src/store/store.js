import { createStore } from 'redux';
import rootReducer from './reducers';

const initialState = {
    msgs: [],
    users: [],
    vChans: [],
    tChans: [],
    hiddenUsers: [],
}

export default createStore(
    rootReducer,
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
