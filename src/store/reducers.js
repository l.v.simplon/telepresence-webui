import {combineReducers} from "redux";

/*
{
  msgs: [
    {
      id: "",
      content: "",
      authorId: "",
      serverId: "",
      channelId: "",
    }
  ],
  users: {
    id: {
      name: "",
      avatar: ""
    }
  },
  servers: {
    id: {
      name: "",
      users: ["id", "id"],
      tChannels: [
        {
          name: "",
          id: ""
        }
      ],
      vChannels: [
        {
          name: "",
          id: ""
        }
      ]
    }
  }
}
*/

function msgs(msgs = [], action) {
  switch(action.type) {
    case 'ADD_MSG':
      if (msgs.find(msg => msg.id === action.msg.id))
        return msgs;
      else
        return [
          ...msgs,
          action.msg
        ];
    case 'ADD_MSGS':
    const msgIdsInStore = msgs.map(msg => msg.id);
    const newMsgs = action.msgs.filter(msg => !msgIdsInStore.includes(msg.id))
    return [
      ...msgs,
      ...newMsgs
    ];
    default:
      return msgs;
  }
}

function users(users = [], action) {
  switch(action.type) {
    case 'SET_USER':
      if (users.find(user => user.id === action.user.id))
        return users;
      else
        return [ ...users, action.user ];
    case 'SET_USERS':
      const userIdsInStore = users.map(user => user.id);
      const newUsers = action.users.filter(user => !userIdsInStore.includes(user.id));
      return [ ...users, ...newUsers ];
    case 'SET_VCSTATUS':
      const usersCopy = [...users];
      const concernedUser = usersCopy.find(user => user.id === action.VcState.userId);
      concernedUser.VcState = action.VcState.state;
      return usersCopy;
    default:
      return users;
  }
}

function vChans(vChans = [], action) {
  switch(action.type) {
    case 'SET_VCHANS':
      const idsInStore = vChans.map(elem => elem.id);
      const newItems = action.vChans.filter(elem => !idsInStore.includes(elem.id));
      return [ ...vChans, ...newItems ];
    default:
      return vChans;
  }
}

function tChans(tChans = [], action) {
  switch(action.type) {
    case 'SET_TCHANS':
      const idsInStore = tChans.map(elem => elem.id);
      const newItems = action.tChans.filter(elem => !idsInStore.includes(elem.id));
      return [ ...tChans, ...newItems ];
    default:
      return tChans;
  }
}

function hiddenUsers(hiddenUserIds = [], action) {
  const hidenUsersIds_set = new Set( hiddenUserIds );
  switch(action.type) {
    case 'HIDE_USER':
      hidenUsersIds_set.add(action.userId);
      return [...hidenUsersIds_set];
    case 'UNHIDE_USER':
      hidenUsersIds_set.delete(action.userId);
      return [...hidenUsersIds_set];
    case 'SET_HIDDEN_USERS':
      return [...action.userIds];
    default:
      return hiddenUserIds;
  }
}

export default combineReducers({
  msgs,
  users,
  vChans,
  tChans,
  hiddenUsers,
});
