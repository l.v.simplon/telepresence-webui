import React from 'react';
import ListGroup from "react-bootstrap/ListGroup";

export default function({vchan, users}) {
    return <ListGroup className="my-2">
        <ListGroup.Item className="py-1" variant={users.length > 0 ? "primary" : "secondary"} key={users.length}>
            <span role="img" aria-label={`${vchan.name} (salon vocal)`}>🔊</span> {vchan.name}
        </ListGroup.Item>
        {
            users
            .map((user, index) =>
                <ListGroup.Item className="py-0" key={index}>
                    {user.name}
                </ListGroup.Item>
            )
        }
            
    </ListGroup>
}
