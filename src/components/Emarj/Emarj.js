import React, { useState, useEffect, useReducer } from 'react';
import { useDispatch } from 'react-redux';
import moment from "moment";

import Spinner from 'react-bootstrap/Spinner';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import DatePicker from "./DatePicker";
import EmarjTable from "./EmarjTable";

import { getMsgs } from "../../api";
import { addMessages } from "../../store/actions";
import { useGetTextChannelsForServer } from "../../hooks";
import { updateQueryParams, useQuery } from "../../queryParamsMngmt";
import {
  initClockingTimes,
  serialize as serializeClockingTimes,
  updateClockingDates
} from "../../clockingTimesMgr";

const ONE_DAY_IN_MS = 24 * 60 * 60 * 1000;
function dateReducer(state, action) {
  switch (action.type) {
    case 'next':
      return new Date(state.getTime() + ONE_DAY_IN_MS);
    case 'prev':
      return new Date(state.getTime() - ONE_DAY_IN_MS);
    case 'today':
      const date = new Date();
      date.setUTCHours(0, 0, 0);
      return date;
    default:
      throw new Error();
  }
}

export default function Emarj({serverId}) {
  const query = useQuery();
  const startDate = query.has("date") ? new Date(query.get("date")) : new Date();
  startDate.setUTCHours(0, 0, 0);

  const tChans = useGetTextChannelsForServer(serverId);

  const startChannel = query.get("channel") ? query.get("channel") : "";
  const [discordChannel, setDiscordChannel] = useState(startChannel);
  useEffect(() => {
    if (!discordChannel && tChans.length > 0)
      setDiscordChannel(tChans[0].id)
  }, [tChans, discordChannel])

  const [date, dispatchDate] = useReducer(dateReducer, startDate);
  const [discordMode, setDiscordMode] = useState(false);
  const [status, setStatus] = useState({loading: true, channelAvailable: null});

  const dispatch = useDispatch();

  useEffect(() => {
    if (discordChannel) {
      setStatus({loading: true, channelAvailable: null});
      getMsgs(date.toISOString(), discordChannel)
        .then((data) => {
          setStatus({loading: false, channelAvailable: data.channelAvailable});

          if (data.messages)
            dispatch(addMessages(data.messages));
        });
    }
  }, [date, discordChannel, dispatch]);

  //clockingDates are an array of dates (so a year+month+day and hour+minutes)
  //used for header display and calculating messages' distance in time
  const [clockingDates, setClockingDates] = useState(initClockingTimes(date));
  //they're re-calculated when date changes (via the DatePicker)
  useEffect(() => setClockingDates(updateClockingDates(clockingDates, date)), [date])

  const setClockingDate = ((index, clockingDate) => {
    const tmp = [...clockingDates];
    tmp[index] = clockingDate;

    setClockingDates(updateClockingDates(tmp, date));
  })

  //update url when date changes
  useEffect(() => updateQueryParams("date", moment(date).format("YYYY-MM-DD")), [date]);
  useEffect(() => updateQueryParams("channel", discordChannel), [discordChannel]);
  useEffect(() => updateQueryParams("times", serializeClockingTimes(clockingDates)), [clockingDates]);

  return <Container fluid>
    <Row>
      <Col xs="auto">
        <label>
          <input type="checkbox" value={discordMode} onChange={(e) => setDiscordMode(e.target.checked)} />
          DISCORD MODE
        </label>
        <DatePicker date={date} dispatchDate={dispatchDate} />
      </Col>

      <Col xs="auto">
        <Form onChange={e => setDiscordChannel(e.target.value)}>
          <Form.Group>
            <Form.Label>
              Channel id (<span>{status.loading ? "loading..." : status.channelAvailable === false ? "channel not available!" : "OK"}</span>)
            </Form.Label>
            <Form.Control as="select" defaultValue={discordChannel} >
              {tChans.map((tChan, index) =>
                <option key={index} value={tChan.id}>#{tChan.name}</option>
              )}
            </Form.Control>
          </Form.Group>
        </Form>
      </Col>
    </Row>

    {status.loading
      ? <Spinner animation="border" role="status" />
      : <EmarjTable
        clockingDates={clockingDates}
        date={date}
        setClockingDate={setClockingDate}
        discordChannel={discordChannel}
        discordMode={discordMode}
      />
    }
  </Container>
}
