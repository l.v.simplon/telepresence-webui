import React from 'react';
import UserMsg from "./UserMsg";

export default function UserRow({user, userMsgs, clockingDates, discordMode}) {
  const msgsBucketedbyClockingDates  = bucketMsgs(clockingDates, userMsgs);

  return (
    <tr>
      <th>{user.name}</th>
      {
        (() => {
          const ret = [];
          for (let index = 0; index < clockingDates.length; index++) {
            const clockingDate = clockingDates[index];
            const bucket = msgsBucketedbyClockingDates[index];

            var msg = bucket[0];
            if (bucket.length > 1)
              msg = getClosest(clockingDate, bucket);

            ret.push(<UserMsg key={index} msg={msg} user={user} clockingDate={clockingDate} discordMode={discordMode}/>);
          }

          return ret;
        })()
      }
    </tr>
  );
}

function bucketMsgs(bucketsTimes = [], msgs = []) {
  const buckets = bucketsTimes.map(e => []);

  msgs.forEach((msg) => {
    const distances = bucketsTimes.map(time => getDistance(time, msg.date))
    const closestTime = distances.reduce(
      (prevState, currDistance, i) => currDistance < prevState.distance ? {distance: currDistance, index: i} : prevState,
      {distance: Infinity, index: -1}
    )
    buckets[closestTime.index].push(msg);
  })

  return buckets;
}

function getDistance(date1, date2) {
  date1 = Number(date1);
  date2 = Number(date2);

  return date1 > date2 ? date1 - date2 : date2 - date1;
}

function getClosest(target, msgs) {
  const {msg} = msgs.reduce((prevState, currMsg) => {
    const currDistance = getDistance(target, currMsg.date);
    return currDistance < prevState.distance ? {distance: currDistance, msg: currMsg} : prevState
  }, { distance: Infinity, msg: null })
  return msg;
}