import React from 'react';
import DiscordMessage from "./Message/DiscordMessage";
import ColoredMessage from "./Message/ColoredMessage";

export default function UserMsg({user, msg, clockingDate, discordMode}) {
  if (!msg)
    return <td disabled>no data</td>

  if (discordMode)
    return <DiscordMessage user={user} msg={msg} clockingDate={clockingDate} />
  else
    return <ColoredMessage user={user} msg={msg} clockingDate={clockingDate} />
}
