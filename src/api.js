import moment from "moment";

export function getUserMsgs(userId, date) {
    return fetch(`/msgs/${userId}?d=${moment(date).format('YYYY-MM-DD')}`);
}

export function getInitData(date, serverId) {
    return fetch(`/initData?d=${moment(date).format('YYYY-MM-DD')}&s=${serverId}`);
}

export async function getMsgs(date, channelId) {
    const res = await fetch(`/amsgs?d=${moment(date).format('YYYY-MM-DD')}&c=${channelId}`);
    return res.json();
}

export function getSSEEventSource(serverId) {
    return new EventSource(`/stream?s=${serverId}`);
}