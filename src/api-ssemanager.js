import {getSSEEventSource} from "./api";
import { addMessage, setVcStatus } from "./store/actions";

export default function(serverId, dispatch) {
  const evtSrc = getSSEEventSource(serverId)

  evtSrc.onmessage = function(event) {
    console.log("server message", event);
  }

  evtSrc.onerror = function(event) {
    console.log("server error", event);
  }

  evtSrc.addEventListener("dmessage", (e) => {
    console.log("discord message", e);
    dispatch(addMessage(JSON.parse(e.data)));
  });

  evtSrc.addEventListener("voiceupdate", (e) => {
    dispatch(setVcStatus(JSON.parse(e.data)));
  })

  return () => evtSrc.close();
}
