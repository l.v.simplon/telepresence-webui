import moment from "moment";

export function readFromLocation() {
    const query = new URLSearchParams(window.location.search);
    if (query.has("times")) {
        return readSerialized(query.get("times"));
    }

    return null;
}

export function serialize(clockingDates) {
    return clockingDates
        .map(date => moment(date).format("HH:mm"))
        .join(",")
}

function readSerialized(str) {
    return str
        .split(",")
        .filter(str => str !== "Invalid date")
        .map(substr => substr.split(":"))
}

const defaultClockingTimes = "9:00,12:00,13:30,17:30";

export const getClockingTimesAsDates = (clockingTimes, startDate = new Date()) => {
  return clockingTimes.map(time => {
    const clockingDate = moment(startDate);
    clockingDate.hour(time[0]);
    clockingDate.minute(time[1]);

    return clockingDate;
  })
}

export const updateClockingDates = (clockingDates, toDate) => {
    toDate = moment(toDate);
    return clockingDates.map(date => moment(toDate).minutes(date.minutes()).hours(date.hours()));
}

export const initClockingTimes = (startDate) => {
    const clockingTimes = readFromLocation();

    return getClockingTimesAsDates(clockingTimes && clockingTimes.length > 0 ? clockingTimes : readSerialized(defaultClockingTimes), startDate);
}
