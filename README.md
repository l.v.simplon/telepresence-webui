a personal project done while learning react at Simplon.

The goals are to visualize who clocked in and who didn't, along with who is in what vocal channel and who isn't, as to help with the current remote working situation.


how to setup:
- clone the server and the webui (making sure that, when in server, webui is at '../webui')
- set server's discord bot token
- install packages
- build the webui (`yarn build`)
- start server with `node index.js`
- visit localhost:2204/\<serverId\>
- ???
- profit!


things i'm not happy with:
- server having webui's path hardcoded
- server having hardcoded port and doesn't handle case where port is taken
- server being in one file (haha sorry)
- how long the URL gets once you start ignoring a few users
- how the webui manages the url's query string but i dont' know better
- server sends messages and activity status from all servers regardless of who the client subscribed to

regardless, right now it's working well enough for what we need, and I'm also the only one hosting it so my conscience is clear. 

